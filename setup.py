from setuptools import find_packages, setup


setup(
    name='lemon-scaner',
    version='0.1',
    author='Trilan Co.',
    author_email='lemon@trilandev.com',
    description='Application to check the links on the site, css and html validation',
    packages=find_packages(exclude=[
        'fabfile', 'test_project', 'test_project.*']),
    include_package_data=True,
    zip_safe=False,
)
