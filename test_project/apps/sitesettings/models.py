from django.contrib.sites.models import Site
from django.db import models
from django.utils.translation import ugettext_lazy as _


class SiteSettings(models.Model):

    site = models.OneToOneField(Site, related_name='settings')

    class Meta:
        verbose_name = _(u'site settings')
        verbose_name_plural = _(u'site settings')
