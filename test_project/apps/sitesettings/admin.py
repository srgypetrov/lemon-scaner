import lemon

from django.contrib.sites.models import Site
from lemon.admin import SiteExtrAdmin
from .models import SiteSettings


class SiteSettingsInline(lemon.StackedInline):

    model = SiteSettings
    can_delete = False


class SiteAdmin(SiteExtrAdmin):

    inlines = [SiteSettingsInline]
    tabs = True


lemon.site.unregister(Site)
lemon.site.register(Site, SiteAdmin)
