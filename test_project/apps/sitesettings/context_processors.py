from django.contrib.sites.models import RequestSite
from django.utils.functional import SimpleLazyObject

from sitesutils.helpers import get_site
from .models import SiteSettings


def sitesettings(request):
    def get_sitesettings():
        if isinstance(request.site, RequestSite):
            return SiteSettings()
        try:
            return SiteSettings.objects.get(site=get_site(request))
        except SiteSettings.DoesNotExist:
            return SiteSettings()
    return {'sitesettings': SimpleLazyObject(get_sitesettings)}
