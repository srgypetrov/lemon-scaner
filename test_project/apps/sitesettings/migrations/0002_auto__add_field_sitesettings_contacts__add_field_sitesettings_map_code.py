# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'SiteSettings.contacts'
        db.add_column(u'sitesettings_sitesettings', 'contacts',
                      self.gf('django.db.models.fields.TextField')(default=''),
                      keep_default=False)

        # Adding field 'SiteSettings.map_code'
        db.add_column(u'sitesettings_sitesettings', 'map_code',
                      self.gf('django.db.models.fields.TextField')(default=''),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'SiteSettings.contacts'
        db.delete_column(u'sitesettings_sitesettings', 'contacts')

        # Deleting field 'SiteSettings.map_code'
        db.delete_column(u'sitesettings_sitesettings', 'map_code')


    models = {
        u'sites.site': {
            'Meta': {'ordering': "('domain',)", 'object_name': 'Site', 'db_table': "'django_site'"},
            'domain': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'sitesettings.sitesettings': {
            'Meta': {'object_name': 'SiteSettings'},
            'contacts': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'map_code': ('django.db.models.fields.TextField', [], {}),
            'site': ('django.db.models.fields.related.OneToOneField', [], {'related_name': "'settings'", 'unique': 'True', 'to': u"orm['sites.Site']"})
        }
    }

    complete_apps = ['sitesettings']