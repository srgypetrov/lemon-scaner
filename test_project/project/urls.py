import lemon

from django.conf import settings
from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.contrib import admin


admin.autodiscover()

urlpatterns = patterns(
    '',
    url(r'^vacancies/', include('vacancies.urls')),
    url(r'^news/', include('news.urls.list_detail')),
    url(r'^questions/', include('questions.urls')),
    url(r'^articles/', include('articles.urls.list_detail')),
    url(r'^captcha/', include('captcha.urls')),
    url(r'^admin/', include(lemon.site.urls)),
) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
