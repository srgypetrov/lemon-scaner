from __future__ import unicode_literals

from django.contrib.admin.util import (
    lookup_field, display_for_field)
from django.contrib.admin.views.main import EMPTY_CHANGELIST_VALUE
from django.core.exceptions import ObjectDoesNotExist
from django.utils.html import format_html
from django.utils.safestring import mark_safe
from django.utils.encoding import force_text
from django.template import Library
from django.contrib.admin.templatetags.admin_list import (
    ResultList, result_hidden_fields, result_headers)


register = Library()


def items_for_result(cl, result, form):
    first = True
    f, attr, status = lookup_field('status', result, cl.model_admin)
    f, attr, redirect = lookup_field('history', result, cl.model_admin)
    if redirect != '0':
        redirect = 'redirect'
    else:
        redirect = ''
    for field_name in cl.list_display:
        try:
            f, attr, value = lookup_field(field_name, result, cl.model_admin)
        except ObjectDoesNotExist:
            result_repr = EMPTY_CHANGELIST_VALUE
        else:
            result_repr = display_for_field(value, f)
        if force_text(result_repr) == '':
            result_repr = mark_safe('&nbsp;')
        if (first and not cl.list_display_links) or field_name in cl.list_display_links:
            table_tag = 'th'
            yield format_html(
                '<{0}><a class="{2} {2}_{3}_{4}" target="_blank" href="{1}">{1}</a></{0}>',
                table_tag,
                result_repr,
                field_name,
                status,
                redirect
            )
        else:
            if (form and field_name in form.fields and not (
                    field_name == cl.model._meta.pk.name and
                    form[cl.model._meta.pk.name].is_hidden)):
                        result_repr = mark_safe(form[field_name])
            yield format_html(
                '<td class="{1} {1}_{2}_{3}">{0}</td>',
                result_repr,
                field_name,
                status,
                redirect
            )


def results(cl):
    for res in cl.result_list:
        yield ResultList(None, items_for_result(cl, res, None))


@register.inclusion_tag("admin/change_list_results.html")
def result_links_list(cl):
    headers = list(result_headers(cl))
    num_sorted_fields = 0
    for h in headers:
        if h['sortable'] and h['sorted']:
            num_sorted_fields += 1
    return {'cl': cl,
            'result_hidden_fields': list(result_hidden_fields(cl)),
            'result_headers': headers,
            'num_sorted_fields': num_sorted_fields,
            'results': list(results(cl))}


@register.filter
def lookup(dictionary, key):
    return dictionary.get(key)
