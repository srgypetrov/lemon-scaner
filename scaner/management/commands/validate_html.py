from django.core.management.base import NoArgsCommand
from scaner.base import start_validate


class Command(NoArgsCommand):

    help = "Validate HTML pages and store information about errors in the database"

    def handle_noargs(self, **options):
        start_validate()
        print "Done"
