from django.core.management.base import NoArgsCommand
from scaner.base import start_scan


class Command(NoArgsCommand):

    help = "Scanning URL's to detect and store information about them in the database"

    def handle_noargs(self, **options):
        start_scan()
        print "Done"
