from django.db import models
from django.utils.translation import ugettext_lazy as _

TYPE = (
    ('internal', _('internal')),
    ('external', _('external')),
    ('broken', _('broken')),
)


class Link(models.Model):

    link = models.CharField(_('link'), max_length=255)
    status = models.CharField(_('status'), max_length=50)
    history = models.CharField(_('history'), max_length=255)
    url = models.CharField(_('url'), max_length=255)
    nesting_level = models.PositiveIntegerField(_('nesting level'))
    type = models.CharField(_('type'), max_length=255, choices=TYPE)

    class Meta:
        ordering = ['status', 'nesting_level']
        verbose_name = _('link')
        verbose_name_plural = _('links')

    def __unicode__(self):
        return self.link


class Error(models.Model):

    link = models.ForeignKey(Link, related_name='errors', blank=True)
    text = models.TextField(_('text'), blank=True, null=True)
    place = models.CharField(_('place'), max_length=255, blank=True)
    type = models.CharField(_('type'), max_length=50, blank=True)

    class Meta:
        ordering = ['link']
        verbose_name = _('validation error')
        verbose_name_plural = _('validation errors')

    def __unicode__(self):
        return self.text


class DateStatistic(models.Model):

    date = models.DateField(_('date'), auto_now_add=True)
    validation_errors = models.PositiveIntegerField(_('validation errors'))
    broken_links = models.PositiveIntegerField(_('broken links'))

    class Meta:
        ordering = ['date']
        verbose_name = _('statistic')
        verbose_name_plural = _('statistics')

    def __unicode__(self):
        return str(self.date)
