from scaner.models import Link, Error, DateStatistic
from itertools import chain
from django.shortcuts import render


def graph_last(request):
    links = get_links()
    types = set(Error.objects.values_list('type', flat=True))
    errors = get_dict_errors(links, types)
    size = len(links) * 70
    colors = {'Error': '#B41A0C', 'Information': '#2F7ED8', 'Warning': '#CC9900'}
    scan_result = get_list_broken(links)
    dates = DateStatistic.objects.all()
    return render(request, 'admin/graph_last.html', {
        'object_list': links,
        'errors': errors,
        'size': size,
        'colors': colors,
        'broken': scan_result[1],
        'links': scan_result[0],
        'dates': dates
    })


def get_links():
    links = Link.objects.exclude(errors__isnull=True)
    extra = get_extra_links()
    return list(chain(links, extra))


def get_dict_errors(links, types):
    d = dict()
    for type in types:
        l = list()
        for link in links:
            count = link.errors.filter(type=type).count()
            l.append(count)
        d[type] = l
    return d


def get_list_broken(links):
    links_count = list()
    links_broken = list()
    for link in links:
        count = Link.objects.filter(url=link.link).count()
        links_count.append(count)
        broken = Link.objects.filter(url=link.link, type='broken').count()
        links_broken.append(broken)
    return links_count, links_broken


def get_extra_links():
    links = Link.objects.filter(type='broken')
    links = links.values_list('url', flat=True).distinct()
    extra = Link.objects.filter(link__in=links)
    return extra


def graph_all(request):
    links = Link.objects.exclude(errors__isnull=True)
    types = set(Error.objects.values_list('type', flat=True))
    errors = get_dict_errors(links, types)
    size = len(links) * 70
    colors = {'Error': '#B41A0C', 'Information': '#2F7ED8', 'Warning': '#CC9900'}
    scan_result = get_list_broken(links)
    return render(request, 'admin/graph.html', {
        'object_list': links,
        'errors': errors,
        'size': size,
        'colors': colors,
        'broken': scan_result[1],
        'links': scan_result[0]
    })
