import requests

from django.contrib.sites.models import Site

from BeautifulSoup import BeautifulSoup
from datetime import datetime
from utils.shortcuts import get_object_or_none

from .models import Link, Error, DateStatistic


current_site = Site.objects.get_current()
base = "http://{0}".format(current_site.domain)
failed = list()


def start_scan():
    #cleaning DB and calling recursive function
    Link.objects.all().delete()
    scan_urls([[base]], 1, [base])
    create_statistic()


def scan_urls(urls, level, parents):

    parsed = list()  # list for store parsed links of this level
    scanned = list()  # list for store parent links for next level

    for block in urls:
        for url in block:
            #get index of parsed links block for preparation of the corresponding parent
            parent_num = urls.index(block)
            links = create_link(url, level, parents[parent_num])
            parsed.append(links)
            scanned.append(url)

    filtered = filter_list(parsed)
    if filtered:
        scan_urls(filtered, level + 1, scanned)  # recursive calling


def create_link(url, level, parent):
    link = ScanningLink(url)  # initialize of scanning link object
    Link.objects.create(  # create link in DB
        url=parent,
        link=link.url,
        status=link.status,
        nesting_level=level,
        type=link.type,
        history=link.history
    )
    # if requests error, add link to failed list
    if link.response.url is None and not link in failed:
        failed.append(link.url)
    return link.links


def filter_list(obj):
    # this func filtering links for next level parse
    exist = list(Link.objects.all().values_list('link', flat=True))
    exist += failed  # exist is a failed + DB entries
    result = list()
    for block in obj:
        l = set(block)  # remove duplicates
        filtered_block = [link for link in l if link not in exist]
        result.append(filtered_block)
        # concatenate exist list for removing duplicates in different links blocks
        exist += filtered_block
    return result


class ScanningLink(object):
    #class for the currently scanning link

    def __init__(self, url):
        self.url = url
        self.response = self.get_response()
        self.soup = self.get_soup()
        self.status = self.get_status()
        self.history = self.get_history()
        self.type = self.get_type()
        self.links = self.get_links()

    def get_response(self):
        # if requests error, default Request object will returned
        default = requests.Request(url=None)
        default.history = None
        default.headers['content-type'] = ''
        # requests stream parameter means that content not downloaded until
        # access the response content. Because of this, big size files (such .pdf) dont
        # take many time
        try:
            return requests.get(self.url, timeout=3, stream=True)
        except requests.exceptions.Timeout:
            default.status_code = '408'
        except Exception:
            default.status_code = '500'
        return default

    def get_status(self):
        return self.response.status_code

    def get_history(self):
        # get redirect codes
        if self.response.history:
            l = list()
            for r in self.response.history:
                l.append(str(r.status_code))
            return ', '.join(l)
        return '0'

    def get_soup(self):
        # access to response only if page is html
        if 'text/html' in self.response.headers['content-type']:
            try:
                return BeautifulSoup(self.response.text)
            except Exception:
                pass
        return None

    def get_type(self):
        if self.status == 404 or self.status == 500:
            return "broken"
        if self.url.startswith(base):
            return 'internal'
        return 'external'

    def get_links(self):
        # get parsed links from soup
        if self.type == 'internal' and self.soup:
            links = [
                dict(a.attrs)['href'] for a in self.soup('a') if 'href' in dict(a.attrs)
            ]
            return self.prepare_urls(links)
        return []

    def prepare_urls(self, objects):
        result = list()
        # filter links, not allow hash, email and etc.
        for obj in objects:
            if obj.startswith('/'):  # for internal links
                result.append(base + obj)
            if obj.startswith('http'):
                result.append(obj)
        return result


# VALIDATION

def start_validate():
    Error.objects.all().delete()
    links = Link.objects.filter(type='internal')
    for num, link in enumerate(links, start=1):
        validate(link)
        procent = (num * 100) / len(links)  # get progress
        print "{0} %".format(procent)
    create_statistic()


def validate(link):
    validation = HTMLValidation(link.link)
    for error in validation.errors:
        if error['text']:
            Error.objects.create(  # create error in DB
                link=link,
                type=error['type'],
                text=error['text'],
                place=error['place']
            )


class HTMLValidation(object):

    validator_url = "http://validator.w3.org/check?uri="
    types = {'msg_warn': 'Warning', 'msg_info': 'Information', 'msg_err': 'Error'}

    def __init__(self, url):
        self.url = url
        self.response = self.get_response()
        self.soup = self.get_soup()
        self.errors = self.validate_html()

    def get_response(self):
        try:
            return requests.get(self.validator_url + self.url, timeout=10)
        except Exception:
            return False

    def get_soup(self):
        if self.response:
            try:
                return BeautifulSoup(self.response.text)
            except Exception:
                pass
        return None

    def validate_html(self):
        errors = list()
        if self.soup:
            errors += self.get_errors('msg_err')
            errors += self.get_errors('msg_info')
            errors += self.get_errors('msg_warn')
        return errors

    def get_errors(self, type):
        l = list()
        for error in self.soup.findAll(True, type):
            try:
                d = self.get_dict(error, type)
            except Exception:
                continue
            l.append(d)
        return l

    def get_dict(self, error, type):
        return {
            'text': error.find(True, "msg").string,
            'type': self.types[type],
            'place': error.em.string
        }


def create_statistic():
    exist = get_object_or_none(DateStatistic, date=datetime.now())
    broken = Link.objects.filter(type='broken').count()
    validation_errors = Error.objects.all().count()
    if not exist:
        DateStatistic.objects.create(
            validation_errors=validation_errors,
            broken_links=broken
        )
    else:
        exist.validation_errors = validation_errors
        exist.broken_links = broken
        exist.save()
