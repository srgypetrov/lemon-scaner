import lemon

from django.utils.translation import ungettext, ugettext_lazy as _
from django.conf.urls.defaults import patterns, url

from .models import Link, Error
from .views import graph_last, graph_all


class ErrorAdmin(lemon.ModelAdmin):

    list_display = ['link', 'text', 'place', 'type']
    list_filter = ['type']
    actions = None
    string_overrides = {
        'changelist_title': _(u'Validation errors'),
        'changelist_paginator_description': lambda n: ungettext('%(count)d error',
                                                                '%(count)d errors', n)
    }

    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None):
        if obj is None:
            return True
        return False


class LinkAdmin(lemon.ModelAdmin):

    change_list_template = 'admin/change_list_links.html'
    list_display = ['link', 'nesting_level', 'status', 'history', 'url']
    list_display_links = ['link', 'url']
    list_filter = ['nesting_level', 'status', 'type']
    actions = None
    string_overrides = {
        'changelist_title': _(u'Links'),
        'changelist_paginator_description': lambda n: ungettext('%(count)d link',
                                                                '%(count)d links', n)
    }

    def get_urls(self):
        urls = super(LinkAdmin, self).get_urls()
        scaner_urls = patterns(
            '',
            url(r'^graph/last/$',
                self.admin_site.admin_view(graph_last),
                name='scaner_graph_last'),
            url(r'^graph/all/$',
                self.admin_site.admin_view(graph_all),
                name='scaner_graph_all'),
        )
        return scaner_urls + urls

    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None):
        if obj is None:
            return True
        return False


lemon.site.register(Link, LinkAdmin)
lemon.site.register(Error, ErrorAdmin)
